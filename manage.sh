#!/bin/bash

LOG="log/nohup.out"
JAVA_OPT="-Xms768m -Xmx768m"
_help() {
        _info "用法：$0 ARG1 ARG2"
        _info "    ARG1: 服务部署目录 | status"
        _info "       默认：NULL(必须)"
        _info "    ARG2: [ start | stop | status | restart ]"
        _info "       默认：status"
        _info "    关于端口:"
        _info "       在项目目录下新建\"port\"文件，写入端口值"
        _info "       默认使用项目内置的端口"
        _info "       $ echo 8888 > project/port"
        _info "       $ $0 project start"
        _info "    "
        echo
}
_time() { echo -n "$(date "+%Y%m%d %H:%M:%S")"; }
_info() {
        echo -e `_time` '\e[0;39;32m'"$1"'\e[0m';
}
_err() {
        echo -e `_time` '\e[0;39;31m'"$1"'\e[0m';
        if [ ! -z "$2" ]; then exit $2; fi
}
_check() {
        # 检查目录下有无pid文件
        local PROJ="$1"
        local pidfile="$PROJ/pid"
        if [ -s "$pidfile" ]; then
                # 修正pid文件
                if [[ "$(cat $pidfile)" =~ ^.*\ .*$ ]]; then
                        for i in $(cat $pidfile); do
                                path="$(readlink -f /proc/$i/cwd)"
                                if [[ "$path" =~ ^.*$PROJ.*$ ]]; then
                                        echo $i > $path/pid
                                fi
                        done
                fi

                if ps -p $(cat "$pidfile") > /dev/null; then
                        return 0
                else
                        : > $pidfile
                        [ $? -eq 0 ] || _err "PID文件操作异常" 5
                        return 1
                fi
        else
                return 1
        fi
}
_ps() {
        # 提供项目名称
        local PROJ="$1"
        local pidfile="$PROJ/pid"
        _check $PROJ
        if [ $? -eq 0 ]; then
                PID="$(cat $pidfile)"
                _info "----------- PROCESS INFO ---------------"
                ps u -p "$PID"
                netstat -plnt 2>/dev/null | sed -n '2p'
                netstat -plnt 2>/dev/null | grep java | grep "$PID"
                _info "----------------------------------------"
                echo
        else
                _err "没找到该项目的进程: $PROJ"
        fi
}
_startup() {
        # 提供项目名称
        local PROJ="$1"
        local PORT=""
        if [ ! -d "$PROJ" ]; then exit 1;fi
        if [ -s "$PROJ/port" ]; then
                PORT="--server.port=$(cat <$PROJ/port)"
        fi
        Time="$2"
        [ -z "$Time" ] && Time=15
        cd $PROJ
        JAR="$(basename *.jar)"
        if [ "x$JAR" == "x*.jar" ]; then
                _err "项目中没找到JAR包!!!" 2
        fi
        _check $PROJ
        if [ $? -eq 0 ]; then
                _err "情况不太好，貌似该项目正在运行" 4
        fi
        _info "项目: $PROJ ..."
        _info "目标程序: $JAR ..."
        if [ ! -d "$(dirname $LOG)" ]; then mkdir -p "$(dirname $LOG)";fi
        trap "echo 等待进程结束" 2
        nohup java $JAVA_OPT -jar $JAR $PORT >> $LOG 2>&1 &
        _info "请等待 ${Time}s 后脚本自行结束"
        sleep 1
        ev=$?;
        PID="$(ps -C java -opid=,cmd= | grep "$JAR" | awk '{print $1}')";
        for i in $PID; do
                if [[ "$(readlink -f /proc/$i/cwd)" =~ ^.*$PROJ.*$ ]]; then
                        PID="$i"
                        break
                fi
        done
        if [ $ev -eq 0 ]; then
                _info "启动中，请稍候..."
        else
                _err "启动异常，请检查日志: $LOG" 2
        fi
        tail -f $LOG &
        sleep $Time
        kill $! >/dev/null 2>&1
        if [[ "$(ps -p $PID -ocmd=)" =~ ^.*$JAR.*$ ]]; then
                _info "启动完成"
                echo $PID > pid
                [ $? -eq 0 ] || _err "PID文件操作异常" 5
        else
                _err "进程失败" 3
        fi
}
_stop() {
        # 提供项目名称
        local PROJ="$1"
        _check $PROJ
        if [ $? -eq 0 ]; then
                _info "即将停止该项目: $PROJ"
                kill -9 $(<"$PROJ/pid") > /dev/null 2>&1
                sleep 3
                _check $PROJ
                if [ $? -eq 0 ]; then
                        _err "貌似停不下来了:("
                        return 2
                else
                        _info "项目已停止 :)"
                        : > $PROJ/pid
                        return 0
                fi
        else
                _info "没有发现相关进程: $PROJ"
                return 1
        fi
}
if [ -z "$1" ]; then _help; exit 0; fi
#if [ ! -d $1 ]; then
#    _err "没找到该项目: $1" 1;
#fi
case $1 in
        status)
                for i in $(ls 2>/dev/null); do
                        if [ -d "$i" ]; then
                                if [ -s "$i/pid" ]; then
                                        _ps $i
                                fi
                        fi
                done
                exit 0
                ;;
        -h|--help)
                _help
                exit 0
                ;;
        *)
                if [ ! -d $1 ]; then
                    _err "没找到该项目: $1" 1;
                fi
                ;;
esac

PROJECT="$(basename $1)";
case $2 in
        start)
                _startup $PROJECT
        ;;
        stop)
                _stop $PROJECT
        ;;
        status|"")
                _ps $PROJECT
        ;;
        restart)
                _stop $PROJECT
                _startup $PROJECT
        ;;
        *)
                _help
        ;;
esac

